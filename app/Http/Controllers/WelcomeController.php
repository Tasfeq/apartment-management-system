<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Contacts;

use Session;

use Illuminate\Http\Request;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$content=view('pages.home_content');
		$slider=view('pages.slider');

		return view('master/master')
		->with('content',$content)
		->with('slider',$slider);
	}


    public function service_view()
	{

		$content=view('pages.service_content');
		
		return view('master/master')->with('content',$content);
	}

   
    public function contact_view()
	{

		$content=view('pages.contact_content');
		
		return view('master/master')->with('content',$content);
	}

	public function our_service()
	{

		$content=view('pages.our_service_content');
		
		return view('master/master')->with('content',$content);
	}

    public function save_contact(Request $request){

        $contact=new Contacts();
        $contact->name=$request->input('name');
        $contact->email=$request->input('email');
        $contact->comment=$request->input('comment');

        $contact->save();

        Session::flash('message','saved successfully');
        return redirect('/contact');



    }

    
    


}
