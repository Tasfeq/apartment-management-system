<?php namespace App\Http\Controllers;


use App\Http\Requests;
use App\User;
use App\Http\Controllers\Controller;
use App\Model\Admins;
use App\Model\Houses;
use App\Model\Expenses;
use App\Model\Apartments;
use App\Model\Tenants;
use App\Model\Messages;
use App\Model\Guests;
use App\Model\Rents;

use Session;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\DB;
use DB;
use Mail;
//use Hash;
class TenantLoginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		 return view('pages.tenant_login_content');
	}

   


    public function logincheck(Request $request)
    {
        $email=$request->input('email');
      
        $pass=$request->input('password');
       // $password=Hash::make('pass');
        
        $info=DB::table('tenants')->where('email',$email)->pluck('password');
        $userinfo=DB::table('tenants')->where('email',$email)->pluck('name');
        $tenantid=DB::table('tenants')->where('email',$email)->pluck('tenant_id');
     /*
       if( Hash::check('pass', $info))
       {

       	return 'valid';
       }
       else{
       	return 'invalid';
       }

     
       $passwordFromPost = $pass;
       $hashedPasswordFromDB = $info;

        if (password_verify($passwordFromPost, $hashedPasswordFromDB)) {
        echo 'Password is valid!';
         } else {
          echo 'Invalid password.';
         }

         exit();
     /*
        
        echo '<pre>';
             print_r($info);
             exit();                     
     */

       if($info)
       {
        Session::put('user',$info);
        Session::put('tenantid',$tenantid);

  
        $content=view('pages.user_content')->with('userinfo',$userinfo);
		
		 return view('master/master')->with('content',$content);
        //   $this->user_profile();   


       }

       else{

       	return 'invalid username';
       }
    }
    
     
     
     public function logout(){

         Session::forget('user');
         return Redirect::to('/');

      }


      public function usermessage(){

        $tenantid=Session::get('tenantid');
        $content=view('pages.user_message_content')->with('tenantid',$tenantid);
		
		return view('master/master')->with('content',$content);

      }


     public function tenantmessage(Request $request){

        $message = new Messages();
	    $tenant_id=$request->input('tenant_id');

	    $message->tenant_id=$request->input('tenant_id');
	    $message->guest=$request->input('guest');
	    $message->stay=$request->input('stay');
	    $message->delay=$request->input('delay');
	    $message->message=$request->input('message');            
	    
        $id=DB::table('tenants')->where('tenant_id',$tenant_id)->pluck('id');

        $message->id=$id;

	    $message->save();


        $tenant_address=DB::table('tenants')->where('tenant_id',$tenant_id)->pluck('email');
        $tenant_name=DB::table('tenants')->where('tenant_id',$tenant_id)->pluck('name');
        $admin_address=DB::table('users')->where('id',$id)->pluck('email');
        
        $data = array( 'from' => $tenant_address,'tenant_name'=>$tenant_name,'to'=>$admin_address );

	    Mail::send('emails.message_admin', $data, function($message) use ($data)
        {
        $message->from($data['from'],$data['tenant_name']);

        $message->to($data['to']);
        });
 
     
	    Session::flash('tenantmessage','Message Sent Successfully');   
	    return redirect('/user-message-send');



     }


     public function userinbox(){

        $tenantid=Session::get('tenantid');
        $all_notifications=DB::table('notifications')->where('tenant_id',$tenantid)->get();

      //  return view('pages.user_inbox_content')->with('all_notifications',$all_notifications);
        $content=view('pages.user_inbox_content')->with('all_notifications',$all_notifications);
		
		return view('master/master')->with('content',$content);

      }

      public function inbox_details($notification_id){

        $notification_id=$notification_id;
        $message=DB::table('notifications')->where('notification_id',$notification_id)->pluck('message');

        $content=view('pages.inbox_details_content')->with('message',$message)
		                                            ->with('notification_id',$notification_id);
	    
	    return view('master/master')->with('content',$content);

      }

     public function notification_delete($notification_id){
        
       DB::table('notifications')->where('notification_id',$notification_id)->delete();
       return redirect('/user-inbox');
  
    }

    
    public function guest_history(){
        
        $tenantid=Session::get('tenantid');
        $apartment_id=DB::table('tenants')->where('tenant_id',$tenantid)->pluck('apartment_id');  
        $all_guests=DB::table('guests')->where('apartment_id',$apartment_id)->get();

        $content=view('pages.guest_history_content')->with('all_guests',$all_guests);
	    
	    return view('master/master')->with('content',$content);

      }

      public function due_expense(){
       
        $tenantid=Session::get('tenantid');
        $apartment_id=DB::table('tenants')->where('tenant_id',$tenantid)->pluck('apartment_id');

        $all_due=DB::table('expenses')->where('apartment_id',$apartment_id)
                                      ->where('clear',0)
                                      ->get(); 
        
        $total=DB::table('expenses')->where('apartment_id',$apartment_id)
                                    ->where('clear',0)
                                    ->sum('total_expense'); 

        $content=view('pages.due_expense_content')->with('all_due',$all_due)
                                                  ->with('total',$total);
		return view('master/master')->with('content',$content);

      }

      public function due_rent(){
       
        $tenantid=Session::get('tenantid');
        $apartment_id=DB::table('tenants')->where('tenant_id',$tenantid)->pluck('apartment_id');

        $all_due=DB::table('rents')->where('apartment_id',$apartment_id)
                                   ->where('clear',0)
                                   ->get(); 
        $content=view('pages.due_rent_content')->with('all_due',$all_due);
		return view('master/master')->with('content',$content);

      }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
