<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use App\Http\Controllers\Controller;
use App\Model\Admins;
use App\Model\Houses;
use App\Model\Expenses;
use App\Model\Apartments;
use App\Model\Tenants;
use App\Model\Guests;
use App\Model\Owners;
use App\Model\Relations;
use App\Model\Rents;

use App\Model\Notifications;
use Session;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\DB;
use DB;
use Mail;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                        if(Auth::check())
                        {
                            $content=view('pages.admin_home_content');
		
		                    return view('master/admin_master')->with('content',$content);
                        }
                        else{
                            return Redirect::to('login');
                        }
	}

    public function add_admin(){

        $content=view('pages.add_admin_content');
        return view('master/admin_master')->with('content',$content);
  
    }

    public function add_house(){
        
        $all_admins = DB::table('users')->where('access_level','!=','1')->get();
        $content=view('pages.add_house_content')->with('all_admins',$all_admins);
        return view('master/admin_master')->with('content',$content);
  
    }


    public function manage_house(){
        
        $all_houses = Houses::all();
        $content=view('pages.manage_house_content')->with('all_houses',$all_houses);
        return view('master/admin_master')->with('content',$content);
  
    }


    public function house_info($house_id){
      //  echo $house_id;
        $house = Houses::find($house_id);
        $id=$house->id;
        $admin = Admins::find($id);
     //   return $houses->house_name;
     //   exit();
     //  echo '<pre>';
    //   print_r($house);
    //  exit();
     //  $house = DB::table('houses')->where('house_id',$house_id)->get();
        $content=view('pages.view_house_content')->with('house',$house)
                                                 ->with('admin',$admin);
        return view('master/admin_master')->with('content',$content);
  
    }

    public function house_edit($house_id){
       
        $house = Houses::find($house_id);
        $all_admins = DB::table('users')->where('access_level','!=','1')->get();
        $content=view('pages.edit_house_content')->with('house',$house)
                                                 ->with('all_admins',$all_admins);
        return view('master/admin_master')->with('content',$content);
  
    }

    public function house_delete($house_id){
        
       DB::table('houses')->where('house_id',$house_id)->delete();
       return redirect('/manage-house');
  
    }

    public function add_apartment(){
    	$all_admins = DB::table('users')->where('access_level','!=','1')->get();
        $all_houses = Houses::all();
        $content=view('pages.add_apartment_content')->with('all_houses',$all_houses)
                                                    ->with('all_admins',$all_admins);
        return view('master/admin_master')->with('content',$content);
  
    }


    public function manage_apartment(){
        
        $all_apartments = DB::table('apartments')
           ->join('users', 'apartments.id', '=', 'users.id')
           ->join('houses', 'apartments.id', '=', 'houses.id')
           ->select('apartments.*', 'houses.house_name','users.name','apartments.apartment_owner','apartments.apartment_id')
           ->get();



       // $all_apartments = Apartments::all();
        $content=view('pages.manage_apartment_content')->with('all_apartments',$all_apartments);
        return view('master/admin_master')->with('content',$content);
  
    }


    public function apartment_info($apartment_id){
      
        $apartment = Apartments::find($apartment_id);
        $content=view('pages.view_apartment_content')->with('apartment',$apartment);
                                                 
        return view('master/admin_master')->with('content',$content);
  
    }

    public function apartment_delete($apartment_id){
        
       DB::table('apartments')->where('apartment_id',$apartment_id)->delete();
       return redirect('/manage-apartment');
  
    }

    public function apartment_edit($apartment_id){
       
        $apartment = Apartments::find($apartment_id);
        $all_admins = DB::table('users')->where('access_level','!=','1')->get();
        $all_houses = Houses::all();
        $content=view('pages.edit_apartment_content')->with('apartment',$apartment)
                                                     ->with('all_houses',$all_houses)
                                                     ->with('all_admins',$all_admins);
        return view('master/admin_master')->with('content',$content);
  
    }


     public function update_apartment(Request $request) {
        
      
        $apartment_id=$request->input('apartment_id');
        $apartment = Apartments::find($apartment_id);
	    
	    $apartment->id=$request->input('admin_id');
	    $apartment->house_id=$request->input('house_id');           
	    $apartment->apartment_owner=$request->input('apartment_owner');
        $apartment->apartment_rent=$request->input('apartment_rent');
        $apartment->apartment_size=$request->input('apartment_size');
        $apartment->apartment_location=$request->input('apartment_location');
	                
	    $apartment->save();

	    
	    return redirect('manage-apartment');
	         
        
    }

    

    public function add_expense(){
        
      //  $all_houses = Houses::all();
      //  $all_apartments = Apartments::all();
        
        $id=Auth::user()->id; 

        $all_houses=DB::table('houses')->where('id',$id)->get();
        $all_apartments=DB::table('apartments')->where('id',$id)->get();
        $content=view('pages.add_expense_content')->with('all_houses',$all_houses)
                                                  ->with('all_apartments',$all_apartments);

        return view('master/admin_master')->with('content',$content);
  
    }

   /*
    public function save_admin(Request $request){

        $user=new App\User();
        $user->name=$request->input('name');
        $user->email=$request->input('email');
        $user->password=Hash::make('password');
        $user->access_level=$request->input('access_level');

        $user->save();

      //  Session::flash('message','saved successfully');
        return redirect('/add-admin');

  

    }

    */
     public function save_house(Request $request) {
        
        $image = $request->file('house_image');
        
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $destination_path = 'house_images/';
            $image_url = $destination_path . $image_full_name;
            $success = $request->file('house_image')->move($destination_path, $image_full_name);

            if ($success) {
              
                $house = new Houses();
                $house->id=$request->input('admin_id');
                $house->house_name = $request->input('house_name');
                $house->house_location = $request->input('house_location');
                $house->house_built = $request->input('house_built');
                
                $house->house_image = $image_url;
                
                $house->save();
                Session::flash('message', 'House Information Saved Successfully !');
                return redirect('add-house');
            }
        }
    }


    public function update_house(Request $request) {
        
        $image = $request->file('house_image');
        
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $destination_path = 'house_images/';
            $image_url = $destination_path . $image_full_name;
            $success = $request->file('house_image')->move($destination_path, $image_full_name);

            if ($success) {
               
                
                $house_id=$request->input('house_id');
                $house = Houses::find($house_id);
                
                $house->house_name = $request->input('house_name');
                $house->house_location = $request->input('house_location');
                $house->house_built = $request->input('house_built');
                $house->house_image = $image_url;
                
                $house->save();

                //Session::flash('message', 'House Information Updated Successfully !');
                return redirect('manage-house');
            }
        }
    }



    public function save_apartment(Request $request){

        $apartment=new Apartments();
        $apartment->id=$request->input('admin_id');
        $apartment->house_id=$request->input('house_id');
        $apartment->apartment_name=$request->input('apartment_name');
        $apartment->apartment_owner=$request->input('apartment_owner');
        $apartment->apartment_rent=$request->input('apartment_rent');
        $apartment->apartment_size=$request->input('apartment_size');
        $apartment->apartment_location=$request->input('apartment_location');

        $apartment->save();

        Session::flash('message','Apartment Information Saved Successfully');
        return redirect('/add-apartment');

  

    }

    public function save_expense(Request $request){
        
        $id=Auth::user()->id; 
        $a_id=$request->input('apartment_id');
        $a_name=DB::table('apartments')->where('apartment_id',$a_id)->pluck('apartment_name');
       // $mail_address=DB::table('tenants')->where('apartment_id',$a_id)->get();
       // $admin_name=DB::table('users')->where('id',$id)->pluck('name');
        
        $expense=new Expenses();
        $expense->id=$id;
        $expense->house_id=$request->input('house_id');
        $expense->apartment_id=$request->input('apartment_id');
        $expense->apartment_name=$a_name;
        $expense->expense_year=$request->input('expense_year');
        $expense->expense_month=$request->input('expense_month');
        $expense->total_expense=$request->input('total_expense');
        //$expense->due=$request->input('due');
 
        $expense->save();
   
        Session::flash('message','Expense Information Saved Successfully');
        return redirect('/add-expense');

    }



    public function add_member(){
        $id=Auth::user()->id;
        $all_apartments=DB::table('apartments')->where('id',$id)->get();
        $admin = Admins::find($id);
        $content=view('pages.add_member_content')->with('all_apartments',$all_apartments)
                                                 ->with('admin',$admin);
        return view('master/admin_master')->with('content',$content);
  
    }


    public function save_member(Request $request){

        $tenant=new Tenants();
        $tenant->apartment_id=$request->input('apartment_id');
        $tenant->id=$request->input('id');
        $tenant->name=$request->input('name');
        $tenant->sex=$request->input('sex');
        $tenant->phone=$request->input('phone');
        $tenant->email=$request->input('email');
        

        $pass=$request->input('password');
        
        $tenant->password=Hash::make('pass');
    /*  echo '<pre>';
        print_r($tenant);
        exit();
    */    
        $tenant->save();

        Session::flash('message','Member Information Saved Successfully');
        return redirect('/add-member');

  

    }


     public function message_to_tenant(){
        
        $id=Auth::user()->id;
        $admin = Admins::find($id);
        $all_tenants=DB::table('tenants')->where('id',$id)->get();
        
        $content=view('pages.message_to_tenant_content')->with('all_tenants',$all_tenants)
                                                        ->with('admin',$admin);
        return view('master/admin_master')->with('content',$content);
  
    }
    

    public function save_message_to_tenant(Request $request){

        $notification=new Notifications();
        $notification->id=$request->input('admin_id');
        $notification->tenant_id=$request->input('tenant_id');
        $notification->title=$request->input('title');
        $notification->message=$request->input('message');

        $notification->save();
        
        $tenant_id=$request->input('tenant_id');
        $id=$request->input('admin_id');
        
        $mail_address=DB::table('tenants')->where('tenant_id',$tenant_id)->pluck('email');
        $admin_name=DB::table('users')->where('id',$id)->pluck('name');

        $data = array( 'email' => $mail_address,'admin_name'=>$admin_name );

        
        Mail::send('emails.message', $data, function($message) use ($data)
        {
        $message->from('tasfeq123@gmail.com', 'AMS');

        $message->to($data['email']);
        });

        Session::flash('message','Message Sent Successfully');
        return redirect('/message-to-tenant');

    }

    
    public function add_guest(){
        
        $id=Auth::user()->id; 

        $all_apartments=DB::table('apartments')->where('id',$id)->get();
        $content=view('pages.add_guest_content')->with('all_apartments',$all_apartments);

        return view('master/admin_master')->with('content',$content);
  
    }


    public function save_guest(Request $request){

        $id=Auth::user()->id; 

        $guest=new Guests();
        $guest->id=$id;
        $guest->apartment_id=$request->input('apartment_id');
        $guest->name=$request->input('name');
        $guest->phone=$request->input('phone');
        $guest->entry=$request->input('entry');
        $guest->out=$request->input('out');
        $guest->save();

        Session::flash('message','Guest Information Saved Successfully');
        return redirect('/add-guest');

    }

    
    public function manage_guest(){
        
        $id=Auth::user()->id; 

        $all_guests=DB::table('guests')->where('id',$id)->get();
        $content=view('pages.manage_guest_content')->with('all_guests',$all_guests);
        return view('master/admin_master')->with('content',$content);
  
    }
    

    public function guest_info($guest_id){
        
        
        $guest = Guests::find($guest_id);
        $apartment_id=DB::table('guests')->where('guest_id',$guest_id)->pluck('apartment_id');
        $apartment_name=DB::table('apartments')->where('apartment_id',$apartment_id)->pluck('apartment_name');
        
        $content=view('pages.guest_info_content')->with('guest',$guest)
                                                 ->with('apartment_name',$apartment_name);
        return view('master/admin_master')->with('content',$content);
  
    }

    public function guest_edit($guest_id){
        
        
        $guest = Guests::find($guest_id);
        $content=view('pages.guest_edit_content')->with('guest',$guest);
        return view('master/admin_master')->with('content',$content);
  
    }

    public function update_guest(Request $request){
        $id=Auth::user()->id;
        $guest_id=$request->input('guest_id');
        $a_id=DB::table('guests')->where('guest_id',$guest_id)->pluck('apartment_id');
        
        $guest = Guests::find($guest_id);

        $guest->id=$id;
        $guest->apartment_id=$a_id;
        $guest->name=$request->input('name');
        $guest->phone=$request->input('phone');
        $guest->entry=$request->input('entry');
        $guest->out=$request->input('out');
        $guest->save();

        Session::flash('message','Guest Information updated Successfully');
        return redirect('/manage-guest');

    }


    public function guest_delete($guest_id){
        
       DB::table('guests')->where('guest_id',$guest_id)->delete();
       return redirect('/manage-guest');
  
    }




    public function add_owner(){
       
        $all_houses = Houses::all();
        
        $content=view('pages.add_owner_content')->with('all_houses',$all_houses);
                                                 
        return view('master/admin_master')->with('content',$content);
  
    }

    
    public function save_owner(Request $request){

        $id=Auth::user()->id; 

        $owner=new Owners();
        $owner->id=$id;
        $owner->house_id=$request->input('house_id');
        $owner->name=$request->input('name');
        $owner->email=$request->input('email');
        $owner->phone=$request->input('phone');
       // $owner->availabe=$request->input('available');
        $owner->save();

        Session::flash('message','Owner Information Saved Successfully');
        return redirect('/add-owner');

    }


    public function add_relation(){
       
        $id=Auth::user()->id; 

        $all_owners=DB::table('owners')->where('id',$id)->get();
        $all_apartments=DB::table('apartments')->where('id',$id)->get();

        $content=view('pages.add_relation_content')->with('all_owners',$all_owners)
                                                   ->with('all_apartments',$all_apartments);
        return view('master/admin_master')->with('content',$content);
  
    }


    public function save_relation(Request $request){

        
        $relation=new Relations();
        
        $relation->owner_id=$request->input('owner_id');
        $relation->apartment_id=$request->input('apartment_id');
        
        $relation->save();

        Session::flash('message','Relation Saved Successfully');
        return redirect('/add-relation');

    }


    public function manage_expense(){
        $id=Auth::user()->id; 
        
        $all_expenses=DB::table('expenses')->where('id',$id)->get();
          
        $content=view('pages.manage_expense_content')->with('all_expenses',$all_expenses);
        return view('master/admin_master')->with('content',$content);
  
    }
    /*
    public function edit_expense($expense_id){
       
        $id=Auth::user()->id; 

        $all_houses=DB::table('houses')->where('id',$id)->get();
        $all_apartments=DB::table('apartments')->where('id',$id)->get();
        $expense_total = DB::table('expenses')->where('apartment_id',$expense_id)->pluck('total_expense');
        $expense_year = DB::table('expenses')->where('apartment_id',$expense_id)->pluck('expense_year');
        $e_id = DB::table('expenses')->where('apartment_id',$expense_id)->pluck('expense_id');
      //  $apartment_name = DB::table('expenses')->where('apartment_id',$expense_id)->pluck('apartment_name');
        
        $content=view('pages.edit_expense_content')->with('all_houses',$all_houses)
                                                   ->with('all_apartments',$all_apartments)
                                                   ->with('expense_total',$expense_total)
                                                   ->with('expense_year',$expense_year)
                                                   ->with('expense_id',$e_id);
                                                 //  ->with('apartment_name',$apartment_name);
        return view('master/admin_master')->with('content',$content);
  
    }

*/

    public function edit_expense($expense_id){
        $id=Auth::user()->id; 
        $all_houses=DB::table('houses')->where('id',$id)->get();
        $all_apartments=DB::table('apartments')->where('id',$id)->get();
         
        $expense = Expenses::find($expense_id);
        $content=view('pages.edit_expense_content')->with('expense',$expense)
                                                   ->with('all_houses',$all_houses)
                                                   ->with('all_apartments',$all_apartments);
        return view('master/admin_master')->with('content',$content);
  

    }
    
    public function update_expense(Request $request){
       
        $id=Auth::user()->id;
        $expense_id=$request->input('expense_id');
        $apartment_id=$request->input('apartment_id');
        $expense_year=$request->input('expense_year');
        $expense_month=$request->input('expense_month');
        $total_expense=$request->input('total_expense');
        $clear=$request->input('clear');

        $a_name=DB::table('apartments')->where('apartment_id',$apartment_id)->pluck('apartment_name');
        $apartment_owner=DB::table('apartments')->where('apartment_id',$apartment_id)->pluck('apartment_owner');
        
        $expense = Expenses::find($expense_id);

        $expense->id=$id;
        $expense->house_id=$request->input('house_id');
        $expense->apartment_id=$request->input('apartment_id');
        $expense->apartment_name=$a_name;
        $expense->expense_year=$request->input('expense_year');
        $expense->expense_month=$request->input('expense_month');
        $expense->total_expense=$request->input('total_expense');
        $expense->clear=$request->input('clear');
        
        $tenant_id=DB::table('tenants')->where('name',$apartment_owner)->pluck('tenant_id');
        $mail_address=DB::table('tenants')->where('tenant_id',$tenant_id)->pluck('email');
        $admin_name=DB::table('users')->where('id',$id)->pluck('name');
        
        if($clear==1)
        {
        
        $data = array( 'email' => $mail_address,'admin_name'=>$admin_name,'expense_year'=>$expense_year,'expense_month'=>$expense_month,'total_expense'=>$total_expense );

        Mail::send('emails.expense_message', $data, function($message) use ($data)
        {
        $message->from('tasfeq123@gmail.com', 'AMS');

        $message->to($data['email']);
        });

        }

        $expense->save();

        Session::flash('message','Expense Information updated Successfully');
        return redirect('/manage-expense');

    }


    public function delete_expense($expense_id){
        
       DB::table('expenses')->where('expense_id',$expense_id)->delete();
       return redirect('/manage-expense');
  
    }

    

    public function add_rent(){
        
        $id=Auth::user()->id; 

        $all_apartments=DB::table('apartments')->where('id',$id)->get();
        $content=view('pages.add_rent_content')->with('all_apartments',$all_apartments);

        return view('master/admin_master')->with('content',$content);
  
    }

    

    public function save_rent(Request $request){
        
        $id=Auth::user()->id; 
        $a_id=$request->input('apartment_id');
        $a_owner=DB::table('apartments')->where('apartment_id',$a_id)->pluck('apartment_owner');
        $amount=DB::table('apartments')->where('apartment_id',$a_id)->pluck('apartment_rent');
        $a_name=DB::table('apartments')->where('apartment_id',$a_id)->pluck('apartment_name');
        $month=$request->input('month');
       
        
        $rent=new Rents();
        $rent->id=$id;
        $rent->apartment_id=$request->input('apartment_id');
        $rent->apartment_name=$a_name;
        $rent->apartment_owner=$a_owner;
        $rent->amount=$amount;
        $rent->month=$request->input('month');
        $rent->year=$request->input('year');
        

        $mail_address=DB::table('tenants')->where('name',$a_owner)->pluck('email');
        $admin_name=DB::table('users')->where('id',$id)->pluck('name');
        
        $data = array( 'email' => $mail_address,'admin_name'=>$admin_name,'amount'=>$amount,'month'=>$month);

        Mail::send('emails.rent_message', $data, function($message) use ($data)
        {
        $message->from('tasfeq123@gmail.com', 'AMS');

        $message->to($data['email']);
        });


        $rent->save();
   
        Session::flash('message','Rent Information Added Successfully and Mail Sent!');
        return redirect('/add-rent');

    }


    public function manage_rent(){
        $id=Auth::user()->id; 
        
        $all_rents=DB::table('rents')->where('id',$id)->get();
          
        $content=view('pages.manage_rent_content')->with('all_rents',$all_rents);
        return view('master/admin_master')->with('content',$content);
  
    }

    
    public function rent_edit($rent_id){
        
        $id=Auth::user()->id; 
        $all_apartments=DB::table('apartments')->where('id',$id)->get();
         
        $rent = Rents::find($rent_id);
        $content=view('pages.edit_rent_content')->with('rent',$rent)
                                                ->with('all_apartments',$all_apartments);
        return view('master/admin_master')->with('content',$content);
  

    }


    public function update_rent(Request $request){
        
        $id=Auth::user()->id; 
        $rent_id=$request->input('rent_id');
        $a_id=$request->input('apartment_id');
        $a_owner=DB::table('apartments')->where('apartment_id',$a_id)->pluck('apartment_owner');
        $amount=DB::table('apartments')->where('apartment_id',$a_id)->pluck('apartment_rent');
        $a_name=DB::table('apartments')->where('apartment_id',$a_id)->pluck('apartment_name');
        $month=$request->input('month');
        $clear=$request->input('clear');
        
        $rent = Rents::find($rent_id);
        $rent->id=$id;
        $rent->apartment_id=$request->input('apartment_id');
        $rent->apartment_name=$a_name;
        $rent->apartment_owner=$a_owner;
        $rent->amount=$amount;
        $rent->month=$request->input('month');
        $rent->year=$request->input('year');
        $rent->clear=$request->input('clear');
        
        
        if($clear==1)
        {

        $mail_address=DB::table('tenants')->where('name',$a_owner)->pluck('email');
        $admin_name=DB::table('users')->where('id',$id)->pluck('name');
        
        $data = array( 'email' => $mail_address,'admin_name'=>$admin_name,'amount'=>$amount,'month'=>$month);

        Mail::send('emails.rent_paid_message', $data, function($message) use ($data)
        {
        $message->from('tasfeq123@gmail.com', 'AMS');

        $message->to($data['email']);
        });

        }

        $rent->save();
   
        Session::flash('message','Rent Status Updated Successfully and Mail Sent To Owner!');
        return redirect('/manage-rent');

    }


    public function rent_delete($rent_id){
        
       DB::table('rents')->where('rent_id',$rent_id)->delete();
       return redirect('/manage-rent');
  
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
