<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

class LoginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                        if(!Auth::check())
                        {
                            return view('auth.login');
                        }
                        else{
                           return      Redirect::to('dashboard');
                        }
	}
         public function Login(Request $request)
        {
            $userData=array('email'=>$request->input('email'),
                'password'=>$request->input('password'));
            $rules=array('email'=>'required',
                'password'=>'required');
            $validator=  Validator::make($userData,$rules);
            if($validator->fails())
            {
               return Redirect::to('login')->withErrors($validator);
            }
            else{
                if(Auth::attempt($userData))
                {
                    $user=Auth::user();
                    Session::put('name',$user->name);
                    Session::put('user_id',$user->id);
                    return Redirect::to('dashbord');
                }
                else{
                    return Redirect::to('login');
                }
            }
        }
        public function checkLogin(Request $request)
        {
            $userData=array('email'=>$request->input('email'),
                'password'=>$request->input('password'));
            $rules=array('email'=>'required','password'=>'required');
             $validation=  Validator::make($userData,$rules);
            if($validation->fails())
            {
               return Redirect::to('login')->withErrors($validation);
            }
            else{
                if(Auth::attempt($userData))
                {
                    $user=Auth::user();
                    Session::put('name',$user->name);
                    Session::put('user_id',$user->id);
                    return Redirect::to('dashboard');
                }
                else{
                    return Redirect::to('login');
                }
            }
        }

        



        /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
