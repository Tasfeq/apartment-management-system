<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('/home', 'WelcomeController@index');

Route::get('/service', 'WelcomeController@service_view');

Route::get('/our-service', 'WelcomeController@our_service');

Route::get('/contact', 'WelcomeController@contact_view');

Route::post('/new-contact', 'WelcomeController@save_contact');

Route::post('/new-admin', 'AdminController@save_admin');

Route::get('/add-admin', 'AdminController@add_admin');

Route::get('/add-house', 'AdminController@add_house');

Route::get('/manage-house', 'AdminController@manage_house');

Route::get('/house-info/{house_id}', 'AdminController@house_info');

Route::get('/house-delete/{house_id}', 'AdminController@house_delete');

Route::get('/house-edit/{house_id}', 'AdminController@house_edit');


Route::get('/add-apartment', 'AdminController@add_apartment');

Route::get('/manage-apartment', 'AdminController@manage_apartment');

Route::get('/apartment-info/{apartment_id}', 'AdminController@apartment_info');

Route::get('/apartment-delete/{apartment_id}', 'AdminController@apartment_delete');

Route::get('/apartment-edit/{apartment_id}', 'AdminController@apartment_edit');

Route::post('/update-apartment', 'AdminController@update_apartment');

Route::get('/add-expense', 'AdminController@add_expense');

Route::get('/manage-expense', 'AdminController@manage_expense');

Route::get('/edit-expense/{expense_id}', 'AdminController@edit_expense');

Route::post('/save-house', 'AdminController@save_house');

Route::post('/update-house', 'AdminController@update_house');

Route::post('/save-apartment', 'AdminController@save_apartment');

Route::post('/save-expense', 'AdminController@save_expense');

Route::post('/update-expense', 'AdminController@update_expense');

Route::get('/delete-expense/{expense_id}', 'AdminController@delete_expense');

Route::get('/add-member', 'AdminController@add_member');

Route::get('/message-to-tenant', 'AdminController@message_to_tenant');

Route::post('/send-tenant-message', 'AdminController@save_message_to_tenant');

Route::post('/save-member', 'AdminController@save_member');

Route::get('/add-guest', 'AdminController@add_guest');
Route::post('/save-guest', 'AdminController@save_guest');
Route::get('/manage-guest', 'AdminController@manage_guest');
Route::get('/guest-info/{guest_id}', 'AdminController@guest_info');
Route::get('/guest-edit/{guest_id}', 'AdminController@guest_edit');
Route::post('/update-guest', 'AdminController@update_guest');
Route::get('/guest-delete/{guest_id}', 'AdminController@guest_delete');

Route::get('/add-rent', 'AdminController@add_rent');
Route::post('/save-rent', 'AdminController@save_rent');
Route::get('/manage-rent', 'AdminController@manage_rent');
Route::get('/rent-edit/{rent_id}', 'AdminController@rent_edit');
Route::post('/update-rent', 'AdminController@update_rent');
Route::get('/rent-delete/{rent_id}', 'AdminController@rent_delete');

Route::get('/add-owner', 'AdminController@add_owner');
Route::post('/save-owner', 'AdminController@save_owner');




Route::get('/add-relation', 'AdminController@add_relation');
Route::post('/save-relation', 'AdminController@save_relation');





Route::get('/tenant-login', 'TenantLoginController@index');

Route::post('/tenant-login', 'TenantLoginController@logincheck');
Route::get('/logout', 'TenantLoginController@logout');

Route::get('/user-message-send', 'TenantLoginController@usermessage');
Route::post('/tenant-message', 'TenantLoginController@tenantmessage');
Route::get('/user-inbox', 'TenantLoginController@userinbox');
Route::get('/inbox-details/{notification_id}', 'TenantLoginController@inbox_details');
Route::get('/notification-delete/{notification_id}', 'TenantLoginController@notification_delete');
Route::get('/guest-history', 'TenantLoginController@guest_history');
Route::get('/due-expense', 'TenantLoginController@due_expense');
Route::get('/due-rent', 'TenantLoginController@due_rent');

Route::get('/login', 'LoginController@index');
Route::post('/auth/login', 'LoginController@checkLogin');
Route::get('/dashboard', 'AdminController@index');

//Route::post('/forgotten-password', 'UserController@ForgottenPassword');

Route::get('user-info',function(){
    $user=new App\User();
    $user->name='ADMIN';
    $user->email='tasfeq123@gmail.com';
    $user->access_level='2';
    $user->password=Illuminate\Support\Facades\Hash::make('123456');
    $user->save();
    
});



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
