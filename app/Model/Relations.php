<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Relations extends Model {

	//
    protected $table='relations';
	public $timestamps=true;
    protected $primaryKey = 'relation_id';
}
