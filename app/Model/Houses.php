<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Houses extends Model {

	protected $table='houses';
	public $timestamps=true;
	protected $primaryKey = 'house_id';

}
