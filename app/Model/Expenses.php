<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model {

	protected $table='expenses';
	public $timestamps=true;
    protected $primaryKey = 'expense_id';
}
