<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Guests extends Model {

	//
    protected $table='guests';
	public $timestamps=true;
	protected $primaryKey = 'guest_id';
}
