<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rents extends Model {

	//
    protected $table='rents';
	public $timestamps=true;
	protected $primaryKey = 'rent_id';
}
