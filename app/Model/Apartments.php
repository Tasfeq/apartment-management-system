<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Apartments extends Model {

	protected $table='apartments';
	public $timestamps=true;
	protected $primaryKey = 'apartment_id';

}
