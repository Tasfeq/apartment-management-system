<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Owners extends Model {

	//
	protected $table='owners';
	public $timestamps=true;
    protected $primaryKey = 'owner_id';

}
