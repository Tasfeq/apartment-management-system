@extends('master.admin_master')


@section('content')

<div class="box-content">
            
        
            {!! Form::open(array('url'=>'save-apartment', 'role'=>'form', 'method'=>'POST')) !!}

              
                <fieldset>
                <legend>ADD Apartment</legend>
                   <h3 style="color: green">
                     {!! Session::get('message')!!}
            
                    </h3>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Apartment Admin<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="admin_id">
                                <option>Select Admin</option>
                                @foreach($all_admins as $v_admins)
                                <option value="{{ $v_admins->id }}" selected="selected">{{ $v_admins->name }}</option>
                                
                              @endforeach
                                
                                
                            </select>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">House Name<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="house_id">
                                <option>Select House Name</option>
                                @foreach($all_houses as $v_houses)
                                <option value="{{ $v_houses->house_id }}" selected="selected">{{ $v_houses->house_name }}</option>
                                
                              @endforeach
                                
                                
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="product">Apartment Name<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="apartment_name" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="product">Owner Name<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="apartment_owner" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Rent<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="apartment_rent" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Size<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="apartment_size" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="textarea">Location<span class="red"> *</span></label>
                        <div class="controls">
                            <textarea style="margin-left: 23.5%;" class="cleditor" name="apartment_location" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>
                                        
            
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        
                    </div>
                </fieldset>
            </form>   
            {!! Form::close() !!}
        </div>


@stop