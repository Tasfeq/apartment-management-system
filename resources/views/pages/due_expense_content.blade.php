
@extends('master.master')


@section('content')

<head>
    <!--
        Charisma v1.0.0

        Copyright 2012 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
    -->
    <meta charset="utf-8">
    <title>User Inbox</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="" href="{{URL::asset('css/bootstrap-cerulean.css')}}" rel="stylesheet">
    <style type="text/css">
      body {
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="{{URL::asset('css/bootstrap-responsive.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/charisma-app.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/jquery-ui-1.8.21.custom.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/fullcalendar.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/fullcalendar.print.css')}}" rel='stylesheet'  media='print'>
    <link href="{{URL::asset('css/chosen.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/uniform.default.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/colorbox.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/jquery.cleditor.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/jquery.noty.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/noty_theme_default.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/elfinder.min.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/elfinder.theme.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/jquery.iphone.toggle.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/opa-icons.css')}}" rel='stylesheet'>
    <link href="{{URL::asset('css/uploadify.css')}}" rel='stylesheet'>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="img/favicon.ico">
        
</head>


<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Members</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Year</th>
                        <th>month</th>
                        <th>total</th>
                        
                        
                    </tr>
                </thead>   
                <tbody>
                   
                   
                    @foreach($all_due as $v_dues)
                     
                    
                    <tr>
                        <td>{{ $v_dues->expense_year }}</td>
                        <td class="center">{{ $v_dues->expense_month }}</td>
                        <td class="center">{{ $v_dues->total_expense }}</td>
                                         
                    
                    </tr>
                        
                    @endforeach 
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>

                    <tr>
                        <td class="center"></td> 
                        <td class="center"><strong>Sub-Total : </strong></td> 
                        <td class="center"><strong><?php echo $total; ?></strong></td>  
                    </tr>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->



<script src="{{URL::asset('js/jquery-1.7.2.min.js')}}"></script>
    <!-- jQuery UI -->
    <script src="{{URL::asset('js/jquery-ui-1.8.21.custom.min.js')}}"></script>
    <!-- transition / effect library -->
    <script src="{{URL::asset('js/bootstrap-transition.js')}}"></script>
    <!-- alert enhancer library -->
    <script src="{{URL::asset('js/bootstrap-alert.js')}}"></script>
    <!-- modal / dialog library -->
    <script src="{{URL::asset('js/bootstrap-modal.js')}}"></script>
    <!-- custom dropdown library -->
    <script src="{{URL::asset('js/bootstrap-dropdown.js')}}"></script>
    <!-- scrolspy library -->
    <script src="{{URL::asset('js/bootstrap-scrollspy.js')}}"></script>
    <!-- library for creating tabs -->
    <script src="{{URL::asset('js/bootstrap-tab.js')}}"></script>
    <!-- library for advanced tooltip -->
    <script src="{{URL::asset('js/bootstrap-tooltip.js')}}"></script>
    <!-- popover effect library -->
    <script src="{{URL::asset('js/bootstrap-popover.js')}}"></script>
    <!-- button enhancer library -->
    <script src="{{URL::asset('js/bootstrap-button.js')}}"></script>
    <!-- accordion library (optional, not used in demo) -->
    <script src="{{URL::asset('js/bootstrap-collapse.js')}}"></script>
    <!-- carousel slideshow library (optional, not used in demo) -->
    <script src="{{URL::asset('js/bootstrap-carousel.js')}}"></script>
    <!-- autocomplete library -->
    <script src="{{URL::asset('js/bootstrap-typeahead.js')}}"></script>
    <!-- tour library -->
    <script src="{{URL::asset('js/bootstrap-tour.js')}}"></script>
    <!-- library for cookie management -->
    <script src="{{URL::asset('js/jquery.cookie.js')}}"></script>
    <!-- calander plugin -->
    <script src="{{URL::asset('js/fullcalendar.min.js')}}"></script>
    <!-- data table plugin -->
    <script src="{{URL::asset('js/jquery.dataTables.min.js')}}"></script>

    <!-- chart libraries start -->
    <script src="{{URL::asset('js/excanvas.js')}}"></script>
    <script src="{{URL::asset('js/jquery.flot.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.flot.pie.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.flot.stack.js')}}"></script>
    <script src="{{URL::asset('js/jquery.flot.resize.min.js')}}"></script>
    <!-- chart libraries end -->

    <!-- select or dropdown enhancer -->
    <script src="{{URL::asset('js/jquery.chosen.min.js')}}"></script>
    <!-- checkbox, radio, and file input styler -->
    <script src="{{URL::asset('js/jquery.uniform.min.js')}}"></script>
    <!-- plugin for gallery image view -->
    <script src="{{URL::asset('js/jquery.colorbox.min.js')}}"></script>
    <!-- rich text editor library -->
    <script src="{{URL::asset('js/jquery.cleditor.min.js')}}"></script>
    <!-- notification plugin -->
    <script src="{{URL::asset('js/jquery.noty.js')}}"></script>
    <!-- file manager library -->
    <script src="{{URL::asset('js/jquery.elfinder.min.js')}}"></script>
    <!-- star rating plugin -->
    <script src="{{URL::asset('js/jquery.raty.min.js')}}"></script>
    <!-- for iOS style toggle switch -->
    <script src="{{URL::asset('js/jquery.iphone.toggle.js')}}"></script>
    <!-- autogrowing textarea plugin -->
    <script src="{{URL::asset('js/jquery.autogrow-textarea.js')}}"></script>
    <!-- multiple file upload plugin -->
    <script src="{{URL::asset('js/jquery.uploadify-3.1.min.js')}}"></script>
    <!-- history.js for cross-browser state change on ajax -->
    <script src="{{URL::asset('js/jquery.history.js')}}"></script>
    <!-- application script for Charisma demo -->
    <script src="{{URL::asset('js/charisma.js')}}"></script>




@stop