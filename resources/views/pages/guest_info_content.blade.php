
@extends('master.admin_master')


@section('content')




<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Members</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Apartment</th>
                        <th>Guest Name</th>
                        <th>Phone</th>
                        <th>Entry Time</th>
                        <th>Leave</th>
                        <th>Entry Date</th>
                        
                    </tr>
                </thead>   
                <tbody>
                   
                   
                    
                    <tr>
                        <td>{{$apartment_name}}</td>
                        <td>{{$guest->name}}</td>
                        <td>{{$guest->phone}}</td>
                        <td class="center">{{$guest->entry}}</td>
                        <td class="center">{{$guest->out}}</td>
                        <td class="center">{{$guest->created_at}}</td>
                        
                    
                    </tr>
                        
                    
                
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

@stop