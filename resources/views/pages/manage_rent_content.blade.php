
@extends('master.admin_master')


@section('content')




<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Members</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Apartment</th>
                        
                        <th>Month</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                   
                   
                    @foreach($all_rents as $v_rents)
                     
                    
                    <tr>
                        <td>{{ $v_rents->apartment_name }}</td>
                        
                        <td class="center">{{ $v_rents->month }}</td>
                       
                        <?php
                        $clear= $v_rents->clear;
                        if($clear==0){
                            $status='Unpaid';
                        }
                        else{
                            $status='Paid';
                        }

                        ?>
                        <td class="center"><?php echo $status; ?> </td>
                        
                    
                        
                        <td class="center">
                         
                         <a class="btn btn-info" href="{{URL::to('/rent-edit/'.$v_rents->rent_id)}}" title="Update">
                                <i class="icon-edit icon-white"></i>  
                                                                        
                         </a>
                         
                         <a class="btn btn-danger" href="{{URL::to('/rent-delete/'.$v_rents->rent_id)}}" title="Delete">
                                <i class="icon-trash icon-white"></i> 
                                
                         </a>
                        
                        </td>
                    
                    </tr>
                        
                    @endforeach 
                
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

@stop