@extends('master.admin_master')


@section('content')

<div class="box-content">
    {!!Form::open(array('url'=>'save-house','role'=>'form','method'=>'POST','files'=>'true'))!!}
        
        <fieldset>
            <legend>ADD House</legend>
            <h3 style="color: green">
            {!! Session::get('message')!!}
            
            </h3>

            <div class="control-group">
                        <label class="control-label" for="typeahead">House Admin<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="admin_id">
                                <option>Select Admin</option>
                                @foreach($all_admins as $v_admins)
                                <option value="{{ $v_admins->id }}" selected="selected">{{ $v_admins->name }}</option>
                                
                              @endforeach
                                
                                
                            </select>
                        </div>
                    </div>


            <div class="control-group">
                <label class="control-label" for="typeahead">House Name</label>
                <div class="controls">
                    <input type="text" name="house_name" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4">
                </div>
            </div>        
            <div class="control-group">
                <label class="control-label" for="typeahead">House Location</label>
                <div class="controls">
                    <textarea cols="100" rows="10" name="house_location" class="span6 typeahead"></textarea>
                </div>
            </div> 

            <div class="control-group">
                <label class="control-label" for="typeahead">House Built</label>
                <div class="controls">
                    <input type="text" name="house_built" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4">
                </div>
            </div> 
            
                    <div class="control-group">
                <label class="control-label" for="typeahead">House Image</label>
                <div class="controls">
                    <input type="file" name="house_image" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4">
                </div>
            </div>
                
            
            <div class="form-actions">
                <input type="submit" class="btn btn-primary"></input>
                
            </div>
        </fieldset>
    {!!Form::close()!!} 
</div>


@stop