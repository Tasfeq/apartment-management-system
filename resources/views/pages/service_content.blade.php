
@extends('master.master')



@section('content')




<div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Login As</h1>
                    <!-- Section's title goes here -->
                    <p></p>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                              <a href="{{URL::to('/login')}}">  <img class="img-circle" src="images/admin.png" alt="service 1"> </a>
                            </div>
                            <h3>Admin</h3>
                            <p></p>
                        </div>
                    </div>
                    <!--div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                              <a href="">  <img class="img-circle" src="images/Service2.png" alt="service 2" /> </a>
                            </div>
                            <h3>Apartment Manager</h3>
                            <p></p>
                        </div>
                    </div-->
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                               <a href="{{URL::to('/tenant-login')}}"> <img class="img-circle" src="images/user.png" alt="service 3"> </a>
                            </div>
                            <h3>User</h3>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@stop