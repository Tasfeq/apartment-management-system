@extends('master.admin_master')


@section('content')

<div class="box-content">
            
        
            {!! Form::open(array('url'=>'update-rent', 'role'=>'form', 'method'=>'POST', 'name'=>'edit_rent_form')) !!}

              
                <fieldset>
                <legend>Update Rent Status</legend>
                   <h3 style="color: green">
                     {!! Session::get('message')!!}
            
                    </h3>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead"><span class="red"> </span></label>
                        <div class="controls">
                            <input type="hidden" name="rent_id" class="span6 typeahead" id="typeahead" value="{{$rent->rent_id}}">
                        </div>
                    </div>



                    <div class="control-group">
                        <label class="control-label" for="typeahead">Apartment Name<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="apartment_id">
                                <option>Select Apartment</option>
                                @foreach($all_apartments as $v_apartments)
                                <option value="{{ $v_apartments->apartment_id }}" selected="selected">{{ $v_apartments->apartment_name }}</option>
                                
                              @endforeach
                                
                                
                            </select>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Month<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="month">
                                
                                
                                <option value="January" selected="selected">January</option>
                                <option value="February" selected="selected">February</option>
                                <option value="March" selected="selected">March</option>
                                <option value="April" selected="selected">April</option>
                                <option value="May" selected="selected">May</option>
                                <option value="June" selected="selected">June</option>
                                
                                <option value="July" selected="selected">July</option>
                                <option value="August" selected="selected">August</option>
                                <option value="September" selected="selected">September</option>
                                <option value="October" selected="selected">October</option>
                                <option value="November" selected="selected">November</option>
                                <option value="December" selected="selected">December</option>
                                
                                <option selected="selected">Select Month</option>
                                
                                
                            </select>
                        </div>
                    </div>
                    
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Year<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="year" class="span6 typeahead" id="typeahead" value="{{$rent->year}}">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Status<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="clear">
                        
                                <option value="1" selected="selected">Paid</option>
                                <option value="0" selected="selected">Unpaid</option>
                                
                            </select>
                        </div>
                    </div>
                  

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        
                    </div>
                </fieldset>
            </form>   
            {!! Form::close() !!}
        </div>


<script type="text/javascript">
document.forms['edit_rent_form'].elements['month'].value='{{$rent->month}}'
</script>

<script type="text/javascript">
document.forms['edit_rent_form'].elements['apartment_id'].value='{{$rent->apartment_id}}'
</script>

<script type="text/javascript">
document.forms['edit_rent_form'].elements['clear'].value='{{$rent->clear}}'
</script>

@stop