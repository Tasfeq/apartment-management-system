@extends('master.admin_master')


@section('content')

<div class="box-content">
            
        
            {!! Form::open(array('url'=>'send-tenant-message', 'role'=>'form', 'method'=>'POST')) !!}

              
                <fieldset>
                <legend>Email to a Tenant</legend>
                   <h3 style="color: green">
                     {!! Session::get('message')!!}
            
                    </h3>

                    <div class="control-group">
                        <label class="control-label" for="product"><span class="red"> </span></label>
                        <div class="controls">
                            <input type="hidden" name="admin_id" class="span6 typeahead" id="typeahead" value="{{$admin->id}}">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Tenant Name<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="tenant_id">
                                <option>Select Tenant</option>
                                @foreach($all_tenants as $v_tenants)
                                <option value="{{ $v_tenants->tenant_id }}" selected="selected">{{ $v_tenants->name }}</option>
                                
                              @endforeach
                                
                                
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="product"><span class="red">Title</span></label>
                        <div class="controls">
                            <input type="text" name="title" class="span6 typeahead" id="typeahead" value="">
                        </div>
                    </div>
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="textarea">Message<span class="red"> *</span></label>
                        <div class="controls">
                            <textarea style="margin-left: 23.5%;" class="cleditor" name="message" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>
                                        
            
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Send</button>
                        
                    </div>
                </fieldset>
            </form>   
            {!! Form::close() !!}
        </div>


@stop