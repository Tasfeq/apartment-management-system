@extends('master.admin_master')


@section('content')

<div class="box-content">
            
        
            {!! Form::open(array('url'=>'save-owner', 'role'=>'form', 'method'=>'POST')) !!}

              
                <fieldset>
                <legend>Create Owner</legend>
                   <h3 style="color: green">
                     {!! Session::get('message')!!}
            
                    </h3>

                    
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">House Name<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="house_id">
                                <option>Select House Name</option>
                                @foreach($all_houses as $v_houses)
                                <option value="{{ $v_houses->house_id }}" selected="selected">{{ $v_houses->house_name }}</option>
                                
                              @endforeach
                                
                                
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="product">Owner Name<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="name" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="product">Owner email<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="email" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">phone<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="phone" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    
                    
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        
                    </div>
                </fieldset>
            </form>   
            {!! Form::close() !!}
        </div>


@stop