
@extends('master.admin_master')


@section('content')




<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Members</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Owner</th>
                        <th>Apartment Rent</th>
                        <th>Apartment Size</th>
                        <th>Apartment Location</th>
                        
                        
                    </tr>
                </thead>   
                <tbody>
                   
             
                    <tr>
                        <td>{{$apartment->apartment_owner}}</td>
                        <td>{{$apartment->apartment_rent}}</td>
                        <td class="center">{{$apartment->apartment_size}}</td>
                        <td class="center">{{$apartment->apartment_location}}</td>
                        
                    </tr>
                        
                    
                
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

@stop