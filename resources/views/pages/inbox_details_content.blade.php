@extends('master.master')


@section('content')


<div id="contact" class="contact">
            <div class="section secondary-section">
                <div class="container">
                    <div class="title">
                        <h1>Message</h1>
                        <p style="color:green;"></p>
                    </div>
                </div>
                <div class="map-wrapper">
                    
                    <div class="container">
                        <div class="row-fluid">
                            <div class="span5 contact-form centered">
                                
                                    
                                    <div class="control-group">
                                        <div class="controls">
                                            <textarea class="span12" name="comment" id="comment" placeholder="* Comments...">{{$message}}</textarea>
                                            <div class="error left-align" id="err-comment"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <div class="controls">
                                         <a href="{{URL::to('/notification-delete/'.$notification_id)}}">  <input class="message-btn" type="submit" name="btn" value="Delete Message" /></a>

                                        </div>
                                    </div>
                                
                               
                            
                                </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>



@stop