@extends('master.admin_master')


@section('content')

<div class="box-content">
            
        
            {!! Form::open(array('url'=>'save-relation', 'role'=>'form', 'method'=>'POST')) !!}

              
                <fieldset>
                <legend>Create Relation</legend>
                   <h3 style="color: green">
                     {!! Session::get('message')!!}
            
                    </h3>


                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Owner Name<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="owner_id">
                                <option>Select Owner Name</option>
                                @foreach($all_owners as $v_owners)
                                <option value="{{ $v_owners->owner_id }}" selected="selected">{{ $v_owners->name }}</option>
                                
                              @endforeach
                                
                                
                            </select>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Apartment Name<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="apartment_id">
                                <option>Select Apartment</option>
                                @foreach($all_apartments as $v_apartments)
                                <option value="{{ $v_apartments->apartment_id }}" selected="selected">{{ $v_apartments->apartment_name }}</option>
                                
                              @endforeach
                                
                                
                            </select>
                        </div>
                    </div>
                                        
            
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        
                    </div>
                </fieldset>
            </form>   
            {!! Form::close() !!}
        </div>


@stop