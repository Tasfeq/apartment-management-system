@extends('master.admin_master')


@section('content')

<div class="box-content">
            
        
            {!! Form::open(array('url'=>'update-guest', 'role'=>'form', 'method'=>'POST')) !!}

              
                <fieldset>
                <legend>Edit Guest Entry</legend>
                   <h3 style="color: green">
                     {!! Session::get('message')!!}
            
                    </h3>
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="product"><span class="red"> </span></label>
                        <div class="controls">
                            <input type="hidden" name="guest_id" class="span6 typeahead" id="typeahead" value="{{$guest->guest_id}}">
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label" for="product">name<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="name" class="span6 typeahead" id="typeahead" value="{{$guest->name}}">
                        </div>
                    </div>
                    
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Phone<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="phone" class="span6 typeahead" id="typeahead" value="{{$guest->phone}}">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Entry<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="entry" class="span6 typeahead" id="typeahead" value="{{$guest->entry}}">
                        </div>
                    </div>
                                        
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Leave<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="out" class="span6 typeahead" id="typeahead" value="{{$guest->out}}">
                        </div>
                    </div>  
                  

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        
                    </div>
                </fieldset>
            </form>   
            {!! Form::close() !!}
        </div>


@stop