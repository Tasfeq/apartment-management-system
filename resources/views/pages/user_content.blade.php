@extends('master.master')

@section('content')



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Uprightness 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20130920

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet" />
<link href="{{URL::asset('default.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{URL::asset('fonts.css')}}" rel="stylesheet" type="text/css" media="all" />

<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->

</head>
<body>

<!--div id="featured">&nbsp;</div-->
<div id="wrapper">
	<div id="page" class="container">
		<div id="content">
			<div class="title">
				<h2>Welcome {{$userinfo}}</h2>
				<span class="byline"></span> </div>
			  
			    <a href="{{URL::to('/user-message-send')}}" class="button">Send Message To Admin</a> </div>
		
	<div id="sidebar">
			<div id="stwo-col">
				
				<div class="sbox2">
					<h2></h2>
					<ul class="style2">
						<li><a href="{{URL::to('/guest-history')}}"><strong><h2>Guest History</h2></strong></a></li>
						<li><a href="{{URL::to('/due-expense')}}"><strong><h2>Due Expenses</h2></strong></a></li>
						<li><a href="{{URL::to('/due-rent')}}"><strong><h2>Due Rents</h2></strong></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div id="featured-wrapper">
		<div id="featured" class="container">
			<div class="major">
				<h2>Check Inbox</h2>
				<span class="byline"></span> </div>
			
			<div class="column1"> <a href="{{URL::to('/user-inbox')}}"><span class="icon icon-inbox"></span></a>
				<div class="title">
					<h2>Inbox</h2>
				</div>
				
			</div>
			
		</div>
	</div>
</div>

</body>
</html>
@stop