@extends('master.admin_master')


@section('content')

<div class="box-content">
            
        
            {!! Form::open(array('url'=>'save-guest', 'role'=>'form', 'method'=>'POST')) !!}

              
                <fieldset>
                <legend>Entry Guest</legend>
                   <h3 style="color: green">
                     {!! Session::get('message')!!}
            
                    </h3>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Apartment Name<span class="red"> *</span></label>
                        
                        <div class="controls">
                             <select class="span6" id="selectError3" name="apartment_id">
                                <option>Select Apartment</option>
                                @foreach($all_apartments as $v_apartments)
                                <option value="{{ $v_apartments->apartment_id }}" selected="selected">{{ $v_apartments->apartment_name }}</option>
                                
                              @endforeach
                                
                                
                            </select>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="product">name<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="name" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Phone<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="phone" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Entry<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="entry" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                                        
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Leave<span class="red"> *</span></label>
                        <div class="controls">
                            <input type="text" name="out" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>  
                  

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        
                    </div>
                </fieldset>
            </form>   
            {!! Form::close() !!}
        </div>


@stop