
@extends('master.admin_master')


@section('content')




<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Members</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>House Name</th>
                        <th>House Admin</th>
                        <th>House Location</th>
                        <th>House built</th>
                        <th>House Image</th>
                        
                    </tr>
                </thead>   
                <tbody>
                   
                   
                   
                    
                    
                    <tr>
                        <td>{{$house->house_name}}</td>
                        <td>{{$admin->name}}</td>
                        <td class="center">{{$house->house_location}}</td>
                        <td class="center">{{$house->house_built}}</td>
                        
                        <td class="center"><img src="{{asset($house->house_image)}}" width="120" height="60" alt="" /></td>
                        
                       
                        
                        
                    
                    </tr>
                        
                    
                
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

@stop