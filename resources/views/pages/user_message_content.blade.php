@extends('master.master')

@section('content')


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Uprightness 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20130920

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet" />
<link href="{{URL::asset('default.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{URL::asset('fonts.css')}}" rel="stylesheet" type="text/css" media="all" />

<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->

</head>
<body>

<!--div id="featured">&nbsp;</div-->
<div id="wrapper">
	<div id="page" class="container">
		
        <div class="row-fluid">
                            <div class="span5 contact-form centered">
                                <h3>Inform Your Apartment Admin</h3>

                                {!! Session::get('tenantmessage')!!}

                                {!!Form::open(array('url'=>'tenant-message','role'=>'form','method'=>'POST'))!!}
                                    

                                    <div class="control-group">
                                        <div class="controls">
                                            <input class="span12" type="hidden" id="name" name="tenant_id" placeholder=""  value="{{$tenantid}}"/>
                                            <div class="error left-align" id="err-name"></div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="control-group">
                                        <div class="controls">
                                            <h4>Guest allowance</h4>
                                            <input class="span12" type="radio" name="guest" value="1" />
                                            <div class="error left-align" id="err-email"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <div class="controls">
                                            <h4>Stay</h4>
                                            <input class="span12" type="radio" name="stay" value="1" />
                                            <div class="error left-align" id="err-email"></div>
                                        </div>
                                    </div>


                                    <div class="control-group">
                                        <div class="controls">
                                            <h4>Delay</h4>
                                            <input class="span12" type="radio" name="delay" value="1" />
                                            <div class="error left-align" id="err-email"></div>
                                        </div>
                                    </div>

                                    
                                    <div class="control-group">
                                        <div class="controls">
                                            <textarea class="span12" rows="12" name="message" id="comment" placeholder="Message.."></textarea>
                                            <div class="error left-align" id="err-comment">Please enter your message.</div>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <div class="controls">
                                           <input class="message-btn" type="submit" name="btn" value="SEND" />
                                           
                                        </div>
                                    </div>
                                
                                {!!Form::close()!!}
                            


                                </div>


                        </div>



		<!--div id="sidebar">
			<div id="stwo-col">
				
				<div class="sbox2">
					<h2></h2>
					<ul class="style2">
						<li><a href="#"><strong>Inbox</strong></a></li>
						
					</ul>
				</div>
			</div>
		</div-->
	</div>
	
	<div id="featured-wrapper">
		<div id="featured" class="container">
			<div class="major">
				<h2>Check Inbox</h2>
				<span class="byline"></span> </div>
			
			<div class="column1"> <a href="{{URL::to('/user-inbox')}}"><span class="icon icon-inbox"></span></a>
				<div class="title">
					<h2>Inbox</h2>
				</div>
				
			</div>
			
		</div>
	</div>
</div>

</body>
</html>


@stop
