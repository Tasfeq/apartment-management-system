
@extends('master.master')



@section('content')




<div id="home">
            <!-- Start cSlider -->
            <div id="da-slider" class="da-slider">
                <div class="triangle"></div>
                <!-- mask elemet use for masking background image -->
                <div class="mask"></div>
                <!-- All slides centred in container element -->
                <div class="container">
                    <!-- Start first slide -->
                    <div class="da-slide">
                        <h2 class="fittext2">Welcome to Apartment Management System</h2>
                        <h4>Secured and Trust worthy</h4>
                        <p> A perfect place to manage your appartment.</p>
                        
                        <div class="da-img">
                            <img src="images/slide2.jpg" alt="image01" width="320">
                        </div>
                    </div>
                    <!-- End first slide -->
                    <!-- Start second slide -->
                    <div class="da-slide">
                        <h2>Find your Apartment for rent</h2>
                        <h4>Updated informartion about apartments within your locality. </h4>
                        <p>Just let us know</p>
                        
                        <div class="da-img">
                            <img src="images/slide1.jpg" width="320" alt="image02">
                        </div>
                    </div>
                    <!-- End second slide -->
                    <!-- Start third slide -->
                    <div class="da-slide">
                        <h2>Keep updated with your apartment manager</h2>
                        <h4>Fast and effective</h4>
                        <p>Inform your apartment manager about your updates immediately from anywhere.</p>
                        
                        <div class="da-img">
                            <img src="images/slide4.jpg" width="320" alt="image03">
                        </div>
                    </div>
                    <!-- Start third slide -->
                    <!-- Start cSlide navigation arrows -->
                    <div class="da-arrows">
                        <span class="da-arrows-prev"></span>
                        <span class="da-arrows-next"></span>
                    </div>
                    <!-- End cSlide navigation arrows -->
                </div>
            </div>
        </div>



 @stop