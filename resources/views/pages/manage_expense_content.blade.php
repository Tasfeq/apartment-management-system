
@extends('master.admin_master')


@section('content')




<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Members</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Apartment Name</th>
                        <th>Expense Year</th>
                        <th>Expense Month</th>
                        <th>Total Expense</th>
                    </tr>
                </thead>   
                <tbody>
                   
                   
                    @foreach($all_expenses as $v_expenses)
                     
                    
                    <tr>
                        <td>{{ $v_expenses->apartment_name }}</td>
                        <td class="center">{{ $v_expenses->expense_year }}</td>
                       
                        <td class="center">{{ $v_expenses->expense_month}}</td>
                        
                        <td class="center">{{ $v_expenses->total_expense}}</td>
                        <!--td class="center"></td-->
                        
                        <td class="center">
                           
                         
                         <a class="btn btn-info" href="{{URL::to('/edit-expense/'.$v_expenses->expense_id)}}" title="Edit">
                                <i class="icon-edit icon-white"></i>  
                                                                        
                         </a>
                         
                         <a class="btn btn-danger" href="{{URL::to('/delete-expense/'.$v_expenses->expense_id)}}" title="Delete">
                                <i class="icon-trash icon-white"></i> 
                                
                         </a>
                        
                        </td>
                    
                    </tr>
                        
                    @endforeach 
                
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

@stop