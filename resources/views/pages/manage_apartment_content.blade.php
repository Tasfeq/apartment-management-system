
@extends('master.admin_master')


@section('content')




<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Members</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Admin Name</th>
                        <th>House Name</th>
                        <th>Apartment Owner</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                   
                   
                    @foreach($all_apartments as $v_apartments)
                     
                    
                    <tr>
                        <td class="center">{{ $v_apartments->name }}</td>
                        <td class="center">{{ $v_apartments->house_name }}</td>
                       
                        
                        <td>{{ $v_apartments->apartment_owner }}</td>
                        
                        <!--td class="center"></td-->
                        
                        <td class="center">
                         
                         <a class="btn btn-green" href="{{URL::to('/apartment-info/'.$v_apartments->apartment_id)}}" title="View" >
                                <i class="icon-home icon-green"></i> 
                                
                         </a>   
                         
                         <a class="btn btn-info" href="{{URL::to('/apartment-edit/'.$v_apartments->apartment_id)}}" title="Edit">
                                <i class="icon-edit icon-white"></i>  
                                                                        
                         </a>
                         
                         <a class="btn btn-danger" href="{{URL::to('/apartment-delete/'.$v_apartments->apartment_id)}}" title="Delete">
                                <i class="icon-trash icon-white"></i> 
                                
                         </a>
                        
                        </td>
                    
                    </tr>
                        
                    @endforeach 
                
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

@stop