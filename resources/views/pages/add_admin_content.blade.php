@extends('master.admin_master')


@section('content')

<div class="box-content">
    {{!!Form::open(array('url'=>'new-admin','role'=>'form','method'=>'POST'))!!}}
        <fieldset>
            <legend>ADD Admin</legend>
            <h3 style="color: green">
            
            </h3>
            <div class="control-group">
                <label class="control-label" for="typeahead">Admin Name</label>
                <div class="controls">
                    <input type="text" name="name" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4">
                </div>
            </div>        
            <div class="control-group">
                <label class="control-label" for="typeahead">Email</label>
                <div class="controls">
                    <input type="text" name="email" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4">
                </div>
            </div> 

            <div class="control-group">
                <label class="control-label" for="typeahead">Password</label>
                <div class="controls">
                    <input type="text" name="password" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4">
                </div>
            </div> 
            
                    <input type="hidden" name="access_level" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" value="2">
                
            
            <div class="form-actions">
                <input type="submit" class="btn btn-primary"></input>
                <button type="reset" class="btn">Reset</button>
            </div>
        </fieldset>
    {{!!Form::close()}} 
</div>


@stop