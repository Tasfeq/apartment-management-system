<!DOCTYPE html>
<!--
 * A Design by GraphBerry
 * Author: GraphBerry
 * Author URL: http://graphberry.com
 * License: http://graphberry.com/pages/license
-->
<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>AMS</title>
        <!-- Load Roboto font -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/bootstrap.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/bootstrap-responsive.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/pluton.css')}}" />
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.cslider.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.bxslider.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/animate.css')}}" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="images/ico/favicon.ico">
    </head>
    
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        <img src="images/houselogo2.jpg" width="120" height="60" alt="Logo" />
                        <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            
                            <?php
                            $user = Session::get('user');
                           

                            if($user)
                             {
                             ?> 
                             
                             <li class=""><a href="{{URL::to('/user-inbox')}}">Profile</a></li>
                             <?php

                              }
                              else{
                             ?>

                            <li class=""><a href="{{URL::to('/home')}}">Home</a></li>

                            <li><a href="{{URL::to('/our-service')}}">Our Service</a></li>
                            
                            <?php
                            }
                            ?>
                             <?php 
                             
                             $user = Session::get('user');

                             if($user)
                             {
                             ?>   
                                <li><a href="{{URL::to('/logout')}}">Logout</a></li>
                             
                             <?php
                             }

                             else
                             {
                             ?>   
                                <li><a href="{{URL::to('/service')}}">Login</a></li>
                             <?php
                             }
                             ?>
                            

                            <li><a href="{{URL::to('/contact')}}">Contact</a></li>

                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        @yield('slider')
        <!-- End home section -->
        <!-- Service section start -->
        @yield('content')
        <!-- Service section end -->
        <!-- Portfolio section start -->
        
                <div class="container">
                    
                    <div class="row-fluid centered">
                        <ul class="social">
                            <li>
                                <a href="">
                                    <span class="icon-facebook-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-twitter-circled"></span>
                                </a>
                            </li>
                            <!--li>
                                <a href="">
                                    <span class="icon-linkedin-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-pinterest-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-dribbble-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-gplus-circled"></span>
                                </a>
                            </li-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact section edn -->
        <!-- Footer section start -->
        
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="{{URL::asset('js/jquery.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/jquery.mixitup.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/bootstrap.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/modernizr.custom.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/jquery.bxslider.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/jquery.cslider.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/jquery.placeholder.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/jquery.inview.js')}}"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="{{URL::asset('js/app.js')}}"></script>
    </body>
</html>