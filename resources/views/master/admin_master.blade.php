<!DOCTYPE html>
<html lang="en">
<head>
	<!--
		Charisma v1.0.0

		Copyright 2012 Muhammad Usman
		Licensed under the Apache License v2.0
		http://www.apache.org/licenses/LICENSE-2.0

		http://usman.it
		http://twitter.com/halalit_usman
	-->
	<meta charset="utf-8">
	<title>AMS Admin Panel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">

	<!-- The styles -->
	<link id="" href="{{URL::asset('css/bootstrap-cerulean.css')}}" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link href="{{URL::asset('css/bootstrap-responsive.css')}}" rel="stylesheet">
	<link href="{{URL::asset('css/charisma-app.css')}}" rel="stylesheet">
	<link href="{{URL::asset('css/jquery-ui-1.8.21.custom.css')}}" rel="stylesheet">
	<link href="{{URL::asset('css/fullcalendar.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/fullcalendar.print.css')}}" rel='stylesheet'  media='print'>
	<link href="{{URL::asset('css/chosen.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/uniform.default.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/colorbox.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/jquery.cleditor.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/jquery.noty.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/noty_theme_default.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/elfinder.min.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/elfinder.theme.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/jquery.iphone.toggle.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/opa-icons.css')}}" rel='stylesheet'>
	<link href="{{URL::asset('css/uploadify.css')}}" rel='stylesheet'>

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="img/favicon.ico">
		
</head>

<body>
		<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid" style="background-color:orange;">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.html">  <span>AMS Admin Panel</span></a>
				
				<!-- theme selector starts -->
				
				<!-- theme selector ends -->
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone">{{ Auth::user()->name }}</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<!--li><a href="#">Profile</a></li-->
						<li class="divider"></li>
						<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Main</li>
						
		<li><a class="ajax-link" href="{{URL::to('/dashboard')}}"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
		
		 <?php
         $access_level=Auth::user()->access_level;
         
         if($access_level==1)
         {
		 ?>
        
		 					
        <li><a class="ajax-link" href="{{URL::to('/add-house')}}"><i class="icon-home"></i><span class="hidden-tablet"> Add House</span></a></li>

        <li><a class="ajax-link" href="{{URL::to('/add-apartment')}}"><i class="icon-home"></i><span class="hidden-tablet"> Add Apartment</span></a></li>


        <li><a class="ajax-link" href="{{URL::to('/manage-house')}}"><i class="icon-home"></i><span class="hidden-tablet"> Manage House</span></a></li>
         
        <li><a class="ajax-link" href="{{URL::to('/manage-apartment')}}"><i class="icon-home"></i><span class="hidden-tablet"> Manage Apartment</span></a></li> 
       

        <?php
        }
        ?>

        <li><a class="ajax-link" href="{{URL::to('/add-owner')}}"><i class="icon-home"></i><span class="hidden-tablet"> Create Owner</span></a></li>

        <li><a class="ajax-link" href="{{URL::to('/add-relation')}}"><i class="icon-home"></i><span class="hidden-tablet"> Owner Apartment Relation</span></a></li>


        <li><a class="ajax-link" href="{{URL::to('/add-expense')}}"><i class="icon-home"></i><span class="hidden-tablet"> Add Expenses</span></a></li>

        <li><a class="ajax-link" href="{{URL::to('/manage-expense')}}"><i class="icon-home"></i><span class="hidden-tablet"> Manage Expense</span></a></li>
        
        <li><a class="ajax-link" href="{{URL::to('/add-member')}}"><i class="icon-home"></i><span class="hidden-tablet"> Add Apartment Members</span></a></li>
        
        
					
        <li><a class="ajax-link" href="{{URL::to('/add-guest')}}"><i class="icon-home"></i><span class="hidden-tablet"> Guest Entry</span></a></li>

        <li><a class="ajax-link" href="{{URL::to('/manage-guest')}}"><i class="icon-home"></i><span class="hidden-tablet"> Manage Guest Entry</span></a></li>

        <li><a class="ajax-link" href="{{URL::to('/message-to-tenant')}}"><i class="icon-home"></i><span class="hidden-tablet"> Email To Tenant</span></a></li> 

        <li><a class="ajax-link" href="{{URL::to('/add-rent')}}"><i class="icon-home"></i><span class="hidden-tablet">Rent</span></a></li>
        
        <li><a class="ajax-link" href="{{URL::to('/manage-rent')}}"><i class="icon-home"></i><span class="hidden-tablet">Manage Rent</span></a></li>
        

					</ul>
					
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			
			@yield('content')
			
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script src="{{URL::asset('js/jquery-1.7.2.min.js')}}"></script>
	<!-- jQuery UI -->
	<script src="{{URL::asset('js/jquery-ui-1.8.21.custom.min.js')}}"></script>
	<!-- transition / effect library -->
	<script src="{{URL::asset('js/bootstrap-transition.js')}}"></script>
	<!-- alert enhancer library -->
	<script src="{{URL::asset('js/bootstrap-alert.js')}}"></script>
	<!-- modal / dialog library -->
	<script src="{{URL::asset('js/bootstrap-modal.js')}}"></script>
	<!-- custom dropdown library -->
	<script src="{{URL::asset('js/bootstrap-dropdown.js')}}"></script>
	<!-- scrolspy library -->
	<script src="{{URL::asset('js/bootstrap-scrollspy.js')}}"></script>
	<!-- library for creating tabs -->
	<script src="{{URL::asset('js/bootstrap-tab.js')}}"></script>
	<!-- library for advanced tooltip -->
	<script src="{{URL::asset('js/bootstrap-tooltip.js')}}"></script>
	<!-- popover effect library -->
	<script src="{{URL::asset('js/bootstrap-popover.js')}}"></script>
	<!-- button enhancer library -->
	<script src="{{URL::asset('js/bootstrap-button.js')}}"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="{{URL::asset('js/bootstrap-collapse.js')}}"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="{{URL::asset('js/bootstrap-carousel.js')}}"></script>
	<!-- autocomplete library -->
	<script src="{{URL::asset('js/bootstrap-typeahead.js')}}"></script>
	<!-- tour library -->
	<script src="{{URL::asset('js/bootstrap-tour.js')}}"></script>
	<!-- library for cookie management -->
	<script src="{{URL::asset('js/jquery.cookie.js')}}"></script>
	<!-- calander plugin -->
	<script src="{{URL::asset('js/fullcalendar.min.js')}}"></script>
	<!-- data table plugin -->
	<script src="{{URL::asset('js/jquery.dataTables.min.js')}}"></script>

	<!-- chart libraries start -->
	<script src="{{URL::asset('js/excanvas.js')}}"></script>
	<script src="{{URL::asset('js/jquery.flot.min.js')}}"></script>
	<script src="{{URL::asset('js/jquery.flot.pie.min.js')}}"></script>
	<script src="{{URL::asset('js/jquery.flot.stack.js')}}"></script>
	<script src="{{URL::asset('js/jquery.flot.resize.min.js')}}"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="{{URL::asset('js/jquery.chosen.min.js')}}"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="{{URL::asset('js/jquery.uniform.min.js')}}"></script>
	<!-- plugin for gallery image view -->
	<script src="{{URL::asset('js/jquery.colorbox.min.js')}}"></script>
	<!-- rich text editor library -->
	<script src="{{URL::asset('js/jquery.cleditor.min.js')}}"></script>
	<!-- notification plugin -->
	<script src="{{URL::asset('js/jquery.noty.js')}}"></script>
	<!-- file manager library -->
	<script src="{{URL::asset('js/jquery.elfinder.min.js')}}"></script>
	<!-- star rating plugin -->
	<script src="{{URL::asset('js/jquery.raty.min.js')}}"></script>
	<!-- for iOS style toggle switch -->
	<script src="{{URL::asset('js/jquery.iphone.toggle.js')}}"></script>
	<!-- autogrowing textarea plugin -->
	<script src="{{URL::asset('js/jquery.autogrow-textarea.js')}}"></script>
	<!-- multiple file upload plugin -->
	<script src="{{URL::asset('js/jquery.uploadify-3.1.min.js')}}"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="{{URL::asset('js/jquery.history.js')}}"></script>
	<!-- application script for Charisma demo -->
	<script src="{{URL::asset('js/charisma.js')}}"></script>
	
		
</body>
</html>

