-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2016 at 04:40 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ams`
--

-- --------------------------------------------------------

--
-- Table structure for table `apartments`
--

CREATE TABLE IF NOT EXISTS `apartments` (
  `apartment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(5) NOT NULL,
  `house_id` int(11) NOT NULL,
  `apartment_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `apartment_owner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apartment_rent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apartment_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apartment_location` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`apartment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `apartments`
--

INSERT INTO `apartments` (`apartment_id`, `id`, `house_id`, `apartment_name`, `apartment_owner`, `apartment_rent`, `apartment_size`, `apartment_location`, `created_at`, `updated_at`) VALUES
(3, 6, 7, 'A1', 'sourav', '10000', '1250', 'rrr', '2015-06-17 01:31:41', '2015-06-17 01:31:41'),
(4, 6, 7, 'A2', 'tt', 'tt', 'ttt', 'tt', '2015-06-17 01:33:04', '2015-06-17 01:33:04'),
(5, 6, 8, 'B1', 'Tasfeq', '50000', '2000', 'qq', '2015-06-17 01:33:35', '2015-06-17 02:34:49'),
(7, 6, 8, 'B2', 'asdsaa', 'sdasdas', 'dasdasd', 'asdasd', '2015-06-17 01:39:31', '2015-06-17 01:39:31'),
(8, 9, 8, 'C1', 'rr', 'rrr', 'rr', 'rrr', '2015-07-01 09:52:05', '2015-07-01 09:52:05'),
(9, 6, 7, 'A43', 'jubair', '20000', '1250', 'south facing and on west side of the lake.', '2015-07-03 02:42:08', '2015-07-03 02:42:08'),
(10, 5, 3, 'C1', 'sayeed', '25000', '1350', 'good', '2015-07-03 13:27:48', '2015-07-03 13:27:48');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `comment`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'tasfeq', 'tasfeq@yahoo.com', 'very good indeed', NULL, '2015-05-31 15:28:33', '2015-05-31 15:28:33'),
(2, 'Eshtiak', 'eshtiak@gmail.com', 'wow... ', NULL, '2015-05-31 15:52:56', '2015-05-31 15:52:56');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
  `expense_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(9) NOT NULL,
  `house_id` int(11) NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `apartment_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `expense_year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expense_month` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_expense` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clear` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`expense_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`expense_id`, `id`, `house_id`, `apartment_id`, `apartment_name`, `expense_year`, `expense_month`, `total_expense`, `clear`, `created_at`, `updated_at`) VALUES
(4, 6, 7, 3, 'A1', '2015', 'January', '2100', '1', '2015-07-03 13:24:51', '2015-07-04 13:17:34'),
(5, 5, 3, 10, 'C1', '2015', 'January', '1200', '0', '2015-07-03 13:28:18', '2015-07-03 13:28:18'),
(11, 6, 7, 3, 'A1', '2015', 'February', '1200', '0', '2015-07-04 12:46:50', '2015-07-04 12:46:50'),
(12, 6, 7, 5, 'B1', '2015', 'August', '3000', '1', '2015-07-04 13:43:04', '2015-07-04 14:01:00'),
(13, 6, 7, 4, 'A2', '2015', 'May', '1200', '0', '2015-07-09 02:46:19', '2015-07-09 02:46:19');

-- --------------------------------------------------------

--
-- Table structure for table `guests`
--

CREATE TABLE IF NOT EXISTS `guests` (
  `guest_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `out` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`guest_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `guests`
--

INSERT INTO `guests` (`guest_id`, `id`, `apartment_id`, `name`, `phone`, `entry`, `out`, `created_at`, `updated_at`) VALUES
(1, 6, 4, 'eshtiak', '00000000', '12:30 pm', '10', '2015-07-02 14:32:04', '2015-07-02 15:34:22'),
(4, 6, 3, 'shahriar', '00000000', '1:00 pm', '3:00 pm', '2015-07-02 16:13:05', '2015-07-02 16:13:05'),
(5, 6, 4, 'sayeed khan', '22222222', '10 pm', '5 am', '2015-07-02 16:22:33', '2015-07-02 16:23:33');

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE IF NOT EXISTS `houses` (
  `house_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(5) NOT NULL,
  `house_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `house_location` text COLLATE utf8_unicode_ci NOT NULL,
  `house_built` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `house_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`house_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `houses`
--

INSERT INTO `houses` (`house_id`, `id`, `house_name`, `house_location`, `house_built`, `house_image`, `created_at`, `updated_at`) VALUES
(1, 5, 'czx', 'tsd', 'zxczx', 'house_images/WjRwkrilYkKc4sL5BE5R.jpg', '2015-06-03 14:18:35', '2015-06-12 00:43:07'),
(3, 5, 'Laravel', 'adad', 'asda', 'house_images/rzLeGMgKbvrSL1THUyOg.jpeg', '2015-06-03 14:21:12', '2015-06-03 14:21:12'),
(7, 6, 'taltola', 'adasd', 'asdasd', 'house_images/xgu9WWJdmXcMgpU3XnTI.jpg', '2015-06-07 00:50:59', '2015-06-07 00:50:59'),
(8, 5, 'banasree', 'asdasd', 'asda', 'house_images/4SnRj5Z5nlowXQSPVbsU.jpg', '2015-06-17 01:36:41', '2015-06-17 01:36:41');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `guest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `id`, `tenant_id`, `guest`, `stay`, `delay`, `message`, `created_at`, `updated_at`) VALUES
(1, 6, 5, '1', NULL, '1', 'hi miraj mama i will be late tonight.... sorry ... bro..', '2015-06-26 13:32:55', '2015-06-26 13:32:55'),
(2, 6, 4, NULL, '1', NULL, 'hi this is majed .. ', '2015-06-26 13:36:12', '2015-06-26 13:36:12'),
(3, 9, 6, '1', '1', '1', 'first email to you sir', '2015-07-01 10:03:41', '2015-07-01 10:03:41'),
(4, 9, 6, '1', '1', '1', 'first email to you sir', '2015-07-01 10:04:00', '2015-07-01 10:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_05_31_201040_create_contacts_table', 1),
('2015_06_03_070507_create_admins_table', 2),
('2015_06_03_191814_create_houses_table', 2),
('2015_06_03_204813_create_apartments_table', 3),
('2015_06_05_061103_create_expenses_table', 4),
('2015_06_24_062534_create_tenants_table', 5),
('2015_06_26_190017_create_messages_table', 6),
('2015_06_26_201326_create_notifications_table', 7),
('2015_07_02_201203_create_guests_table', 8),
('2015_07_03_084356_create_owners_table', 9),
('2015_07_03_095051_create_relations_table', 10),
('2015_07_06_180402_create_rents_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `id`, `tenant_id`, `title`, `message`, `created_at`, `updated_at`) VALUES
(3, 6, 5, 'third message', 'hi there buddy...', '2015-06-30 14:14:42', '2015-06-30 14:14:42'),
(20, 6, 3, 'great', 'hello mate', '2015-06-30 15:39:19', '2015-06-30 15:39:19'),
(24, 6, 3, 'fourth', 'great if done', '2015-06-30 15:55:44', '2015-06-30 15:55:44'),
(30, 6, 7, 'Hey Bro.........itz sexy ................ :)', '<span class="Apple-tab-span" style="white-space:pre">	</span>fjdhfdoijiokljflh', '2015-07-09 02:04:33', '2015-07-09 02:04:33');

-- --------------------------------------------------------

--
-- Table structure for table `owners`
--

CREATE TABLE IF NOT EXISTS `owners` (
  `owner_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(5) NOT NULL,
  `house_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `available` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `owners`
--

INSERT INTO `owners` (`owner_id`, `id`, `house_id`, `name`, `email`, `phone`, `available`, `created_at`, `updated_at`) VALUES
(1, 6, 8, 'sayeed khan', 'sayeed@yahoo.com', '11111111', '0', '2015-07-03 03:45:27', '2015-07-03 03:45:27'),
(2, 6, 8, 'Sir miraj', 'miraj@yahoo.com', '01234789', '0', '2015-07-03 04:20:41', '2015-07-03 04:20:41');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('rahman@gmail.com', 'cc7bdb4831ae741d6103d825c6defff86ceee27c163c7ce6eb29cc3dfdcd92e7', '2015-06-12 15:57:57'),
('tasfeqndc@yahoo.com', '9b843a3d4d818386638ffc36bcd0074243ba52c67784a16890663d4ecac53b56', '2015-06-22 13:56:51');

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE IF NOT EXISTS `relations` (
  `relation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`relation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`relation_id`, `owner_id`, `apartment_id`, `created_at`, `updated_at`) VALUES
(1, 1, 9, '2015-07-03 04:31:38', '2015-07-03 04:31:38'),
(2, 1, 5, '2015-07-03 04:33:32', '2015-07-03 04:33:32');

-- --------------------------------------------------------

--
-- Table structure for table `rents`
--

CREATE TABLE IF NOT EXISTS `rents` (
  `rent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `apartment_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `apartment_owner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `month` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clear` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`rent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE IF NOT EXISTS `tenants` (
  `tenant_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `apartment_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sex` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tenant_id`),
  UNIQUE KEY `tenants_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`tenant_id`, `apartment_id`, `id`, `name`, `sex`, `phone`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 5, 6, 'Tasfeq', 'Male', '0111111111', 'tasfeq@yahoo.com', '$2y$10$i88e1qDYc6RpRLnpoxZDmebmoi.DrLXdL9J5pGJ9pi2jupu..JBh6', NULL, '2015-06-24 01:40:15', '2015-06-24 01:40:15'),
(4, 5, 6, 'Majed', 'Female', '0111111111', 'majed@gmail.com', '$2y$10$gu4mF5KoNJGfmXCuLulGjOI/zkVPq8L7ZDqZl/VwwAuLXd1A/rzk.', NULL, '2015-06-24 01:40:46', '2015-06-24 01:40:46'),
(5, 3, 6, 'sourav ', 'Male', '0000', 'sourav@yahoo.com', '$2y$10$KD7Y3fMnNl8SqyP6xnOjq.qFTmNa5Mp6iDpl7Qs.vvaqD03gWWldu', NULL, '2015-06-24 13:42:22', '2015-06-24 13:42:22'),
(6, 8, 9, 'tasfeq new', 'Male', '0000000', 'tasfeqndc@yahoo.com', '$2y$10$DbEq9NyxiUAdwdV/Rt6i0ugcSNtlBBU4eF.krMmQQDKxFgLZaGQlC', NULL, '2015-07-01 09:53:44', '2015-07-01 09:53:44'),
(7, 4, 6, 'Eshtiak Ahmed', 'Male', '444', 'eshtiakahmed25@gmail.com', '$2y$10$T0UzI4Fy.dpX4F06fdcHj.zHovn1eYmCLoq.1vqKNiG5rRWdJSuHC', NULL, '2015-07-09 00:42:09', '2015-07-09 00:42:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `access_level` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `access_level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tasfequr Rahman', 'rahman@gmail.com', '$2y$10$qYbOsFgAs4UKOuz8WVLmP.t5QdwSq9Bj0adzSy/yZXoKOvE4V33Zu', '1', '0LrgTLSD9ZPNjfV6SNwM0WXasPKiP4WOVpKgdady3QhF4NYjlOGKHikCehJ6', '2015-06-02 11:36:03', '2015-07-09 02:29:32'),
(5, 'Arafat', 'arafat@gmail.com', '$2y$10$rCnT71gkaE.Vy/wEh/foUOD7d.7Bnpsyk7nDbys/zhg1cldaBXKDa', '2', 'OVy91sqjSfn7K3R0gFLeV5rGWFTJ2Nm3Jqut2LYCcdhQqevI2VQL6jv5UnaA', '2015-06-03 01:37:19', '2015-07-03 14:33:37'),
(6, 'Sourav', 'sourav@gmail.com', '$2y$10$8Cw4zDZ01/KJNa46RWFhGuDqUQit.uYvwE21mVOwiod0fFkvzM4Te', '2', '330C7ONujJDAivvujxnQaQ4x3W2k5CVxZw02bD5CgNQ7ks7oWP5DlwTwpMBE', '2015-06-07 00:49:30', '2015-07-09 02:48:41'),
(8, 'Abir', 'tasfeqndc@yahoo.com', '$2y$10$l8lXden/JSmTNRE3ShUdW.JovTs6p9NGga4brlPoNklus2hVgVK72', '1', '2zK1NvelXUP0hhf4rM6c0PUlOlrpHin7TgbacdX3AOiX68OKxwDXS3rMj8kV', '2015-06-17 14:02:02', '2015-07-01 09:50:52'),
(9, 'ADMIN', 'tasfeq123@gmail.com', '$2y$10$HNus5ew9dFx.d0NCzB255.PlawmPRe9gfYBDRzobKETZM5.6NiFc.', '2', 'Vq82ldRtGzhqFc4Ggt6oSeTBTDN36MrF9Ks7LrnvzrORN9anvnq3QdldQwZI', '2015-07-01 09:50:59', '2015-07-01 09:53:50');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
